/*!
\file
\brief Файл запуска приложения.
\author Designer
\version 1.0
\date 09.06.2019
*/
#include "numbersystem.h"
#include <QApplication>

/*!

\brief Функция создаёт и отображает приложение.

\version 1.0

//Полное описание функции:

Главная функция, с котрой начинается выполнение программы,
 создаёт экземпляр класса NumberSystem и показывает его на экране.

\param[in] argv[] - массив указателей на строки
\param[in] argc - содержит количество параметров, передаваемых в функцию main

\return запускает бесконечный цикл обработки событий

*/


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    NumberSystem w;
    w.show();

    return a.exec();
}
