/*!
\file
\brief Файл реализующий перевод чисел из 16 в 8 систему счисления.
\author Designer
\version 1.0
\date 09.06.2019
*/

#include "functions.h"
/*!
\brief Функция перевода из 16 в 8 систему счисления.

\version 1.0

//Полное описание функции:

Функция, выполняющая перевод из шестнадцатеричной системы счисления в восьмеричную.

\param[in] str[] - число, которое нужно перевести из шестнадцатеричной системы счисления, преобразовано в строку

\return возвращает строку в восьмеричной системе счисления

*/
QString from16to8 (QString &str)
{
    QString s = "";
    long k = 1, res = 0, p;

    if(str == "") return "";

    for(int i = str.length()-1; i >= 0; i--)
    {
        if(str[i].toLatin1() >= 'A') p = 55;
        else p = 48;
        res += (str[i].toLatin1()-p) * k;
        k *= 16;
    }

    while(res)
    {
        s.prepend(QString::number(res % 8));
        res /= 8;
    }
    return s;
}
