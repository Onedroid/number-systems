/*!
\file
\brief Файл реализующий перевод чисел из 2 в 8 систему счисления.
\author Designer
\version 1.0
\date 09.06.2019
*/

#include "functions.h"
/*!
\brief Функция перевода из 2 в 8 систему счисления.

\version 1.0

//Полное описание функции:

Функция, выполняющая перевод из двоичной системы счисления в восьмеричную.

\param[in] str[] - число, которое нужно перевести из двоичной системы счисления, преобразовано в строку

\return возвращает строку в восьмеричной системе счисления

*/
QString from2to8 (QString &str)
{
    QString s = "";
    int k = 1, res = 0;

    if(str == "") return "";

    for(int i = str.length()-1; i >= 0; i--)
    {
        res += (str[i].toLatin1()-48) * k;
        k *= 2;
    }

    while(res)
    {
        s.prepend(QString::number(res % 8));
        res /= 8;
    }
    return s;
}
