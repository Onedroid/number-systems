/*!
\file
\brief Файл объявляющий классы и слоты.
\author Designer
\version 1.0
\date 09.06.2019
*/

#ifndef NUMBERSYSTEM_H
#define NUMBERSYSTEM_H

#include <QMainWindow>
#include "functions.h"
#include <QStandardItemModel>

namespace Ui {
class NumberSystem;
}

/*!

\brief Главный класс.

\version 1.0

//Полное описание класса:

Класс, объект которого отображается на экране.
*/
class NumberSystem : public QMainWindow
{
    Q_OBJECT
    QStandardItemModel *model = new QStandardItemModel;

public:
    explicit NumberSystem(QWidget *parent = nullptr);
    ~NumberSystem();

private:
    Ui::NumberSystem *ui;

public slots:
    void converter();
    void box();
    void save();

};

#endif // NUMBERSYSTEM_H
