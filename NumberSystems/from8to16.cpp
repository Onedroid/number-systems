/*!
\file
\brief Файл реализующий перевод чисел из 8 в 16 систему счисления.
\author Designer
\version 1.0
\date 09.06.2019
*/

#include "functions.h"
/*!
\brief Функция перевода из 8 в 16 систему счисления.

\version 1.0

//Полное описание функции:

Функция, выполняющая перевод из восьмеричной системы счисления в шестнадцатеричную.

\param[in] str[] - число, которое нужно перевести из восьмеричной системы счисления, преобразовано в строку

\return возвращает строку в шестнадцатеричной системе счисления

*/
QString from8to16 (QString &str)
{
    QString s = "";
    int k = 1, res = 0;

    if(str == "") return "";

    for(int i = str.length()-1; i >= 0; i--)
    {
        res += (str[i].toLatin1()-48) * k;
        k *= 8;
    }

    while(res)
    {
        if(res % 16 > 9) s.prepend(('A'+(res % 16) - 10));
        else s.prepend(QString::number(res % 16));
        res /= 16;
    }
    return s;
}
