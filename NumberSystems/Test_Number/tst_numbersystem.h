#ifndef TST_NUMBERSYSTEM_H
#define TST_NUMBERSYSTEM_H

#include <QtCore>
#include <QtTest/QtTest>

class Test_Number : public QObject
{
    Q_OBJECT

public:
    Test_Number();

private slots:
    void test_from2to10();
    void test_from10to2();
    void test_from2to8();
    void test_from8to2();
    void test_from8to16();
    void test_from16to8();
    void test_from10to8();
    void test_from8to10();
};

#endif // TST_NUMBERSYSTEM_H
