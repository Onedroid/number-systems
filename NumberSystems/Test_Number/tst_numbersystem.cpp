#include <QtTest>
#include <../functions.h>
#include "tst_numbersystem.h"

Test_Number::Test_Number(){

}

void Test_Number::test_from2to10()
{
    QString test_str = "101010";
    QCOMPARE("42", from2to10(test_str));

    test_str = "";
    QCOMPARE("", from2to10(test_str));

    test_str = "01000";
    QCOMPARE("8", from2to10(test_str));
}

void Test_Number::test_from10to2()
{
    QString test_str = "42";
    QCOMPARE("101010", from10to2(test_str));

    test_str = "";
    QCOMPARE("", from10to2(test_str));

    test_str = "05";
    QCOMPARE("101", from10to2(test_str));
}

void Test_Number::test_from2to8()
{
    QString test_str = "101010";
    QCOMPARE("52", from2to8(test_str));

    test_str = "";
    QCOMPARE("", from2to8(test_str));

    test_str = "01010";
    QCOMPARE("12", from2to8(test_str));
}

void Test_Number::test_from8to2()
{
    QString test_str = "42";
    QCOMPARE("100010", from8to2(test_str));

    test_str = "";
    QCOMPARE("", from8to2(test_str));

    test_str = "05";
    QCOMPARE("101", from8to2(test_str));
}

void Test_Number::test_from8to16()
{
    QString test_str = "4256";
    QCOMPARE("8AE", from8to16(test_str));

    test_str = "";
    QCOMPARE("", from8to16(test_str));

    test_str = "032654";
    QCOMPARE("35AC", from8to16(test_str));
}

void Test_Number::test_from16to8()
{
    QString test_str = "8AE";
    QCOMPARE("4256", from16to8(test_str));

    test_str = "";
    QCOMPARE("", from16to8(test_str));

    test_str = "035AC";
    QCOMPARE("32654", from16to8(test_str));
    //system("pause");
}

void Test_Number::test_from8to10()
{
    QString test_str = "707070";
    QCOMPARE("233016", from8to10(test_str));

    test_str = "";
    QCOMPARE("", from8to10(test_str));

    test_str = "0765";
    QCOMPARE("501", from8to10(test_str));
}

void Test_Number::test_from10to8()
{
    QString test_str = "233016";
    QCOMPARE("707070", from10to8(test_str));

    test_str = "";
    QCOMPARE("", from10to8(test_str));

    test_str = "056";
    QCOMPARE("70", from10to8(test_str));
}
