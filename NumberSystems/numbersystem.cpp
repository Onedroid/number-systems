/*!
\file
\brief Файл реализующий перевод чисел между системами счисления.
\author Designer
\version 1.0
\date 09.06.2019
*/
#include "numbersystem.h"
#include "ui_numbersystem.h"
#include "functions.h"

/*!
\brief Конструктор класса NumberSystem.

\version 1.0

//Полное описание:

Задаёт ограничения на ввод символов и устанавливает слушатели сигналов.
*/
NumberSystem::NumberSystem(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::NumberSystem)
{
    ui->setupUi(this);

    this->setWindowTitle("Системы счисления"); //заголовок

    QRegExp rx("[0-1]{20}");
    ui->lineEdit->setValidator(new QRegExpValidator(rx, this));

    connect(ui->comboBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(box()));
    connect(ui->comboBox_2, SIGNAL(currentIndexChanged(QString)), this, SLOT(converter()));
    connect(ui->lineEdit, SIGNAL(textChanged(QString)), this, SLOT(converter()));
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(save()));

    ui->listView->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

/*!
\brief Метод для списка систем счисления.

\version 1.0

//Полное описание метода:

Метод формирует список возможных для перевода систем счисления из выбранной пользователем системы счисления.
*/
void NumberSystem::box()
{
    ui->lineEdit->setText("");

    QString str;
    QStringList list;

    if(ui->comboBox->currentText() == "2"){
        str = "[0-1]{20}";
        ui->comboBox_2->clear();

        list << "8" << "10";
        ui->comboBox_2->addItems(list);
    }
    else if(ui->comboBox->currentText() == "8")
    {
        str = "[0-7]{9}";
        ui->comboBox_2->clear();

        list << "2" << "10" << "16";
        ui->comboBox_2->addItems(list);
    }
    else if(ui->comboBox->currentText() == "10")
    {
        str = "[0-9]{9}";
        ui->comboBox_2->clear();

        list << "2" << "8";
        ui->comboBox_2->addItems(list);
    }
    else if(ui->comboBox->currentText() == "16")
    {
        str = "[A-F0-9]{7}";
        ui->comboBox_2->clear();

        list << "8";
        ui->comboBox_2->addItems(list);
    }

    QRegExp rx(str);
    ui->lineEdit->setValidator(new QRegExpValidator(rx, this));
}

/*!

\brief Метод для перевода систем счисления.

\version 1.0

//Полное описание метод:

Метод переводит числа при заданных системах счисления.
*/

void NumberSystem::converter()
{
    if(ui->comboBox->currentText() == "2")
    {
           QString s = ui->lineEdit->text();
           if(ui->comboBox_2->currentText() == "8")
               s = from2to8(s);
           else if(ui->comboBox_2->currentText() == "10")
               s = from2to10(s);
           ui->lineEdit_2->setText(s);
    }
    else if(ui->comboBox->currentText() == "8")
    {
           QString s = ui->lineEdit->text();
           if(ui->comboBox_2->currentText() == "2")
               s = from8to2(s);
           else if(ui->comboBox_2->currentText() == "16")
               s = from8to16(s);
           else if(ui->comboBox_2->currentText() == "10")
               s = from8to10(s);
           ui->lineEdit_2->setText(s);
    }
    else if(ui->comboBox->currentText() == "10")
    {
        if(ui->comboBox_2->currentText() == "2")
        {
           QString s = ui->lineEdit->text();
           ui->lineEdit_2->setText(from10to2(s));
        }
        else if(ui->comboBox_2->currentText() == "8")
        {
            QString s = ui->lineEdit->text();
            ui->lineEdit_2->setText(from10to8(s));
        }
    }
    else if(ui->comboBox->currentText() == "16")
    {
           QString s = ui->lineEdit->text();
           ui->lineEdit_2->setText(from16to8(s));
    }
}

/*!

\brief Метод сохранения.

\version 1.0

//Полное описание метод:

Метод сохраняет результаты вычислений при нажатии на кнопку "Сохранить".
*/
void NumberSystem::save()
{
    model->appendRow(new QStandardItem(QString(ui->lineEdit->text() + "("  + ui->comboBox->currentText() + ") -> " + ui->lineEdit_2->text() + "(" + ui->comboBox_2->currentText() + ")")));
    ui->listView->setModel(model);
}

/*!
\brief Деструктор класса NumberSystem.

\version 1.0

//Полное описание:

Очищает память.
*/
NumberSystem::~NumberSystem()
{
    delete ui;
}
