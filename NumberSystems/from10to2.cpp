/*!
\file
\brief Файл реализующий перевод чисел из 10 в 2 систему счисления.
\author Designer
\version 1.0
\date 09.06.2019
*/

#include "functions.h"
/*!
\brief Функция перевода из 10 в 2 систему счисления.

\version 1.0

//Полное описание функции:

Функция, выполняющая перевод из десятичной системы счисления в двоичную.

\param[in] str[] - число, которое нужно перевести из десятичной системы счисления, преобразовано в строку

\return возвращает строку в двоичной системе счисления

*/
QString from10to2 (QString &str)
{
    QString s = "";
    int res = str.toInt();

    if(str == "") return "";

    while(res)
    {
        s.prepend(QString::number(res % 2));
        res /= 2;
    }

    return s;
}
