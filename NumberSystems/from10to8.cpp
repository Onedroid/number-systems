/*!
\file
\brief Файл реализующий перевод чисел из 10 в 8 систему счисления.
\author Designer
\version 1.0
\date 09.06.2019
*/

#include "functions.h"
/*!
\brief Функция перевода из 10 в 8 систему счисления.

\version 1.0

//Полное описание функции:

Функция, выполняющая перевод из десятичной системы счисления в восьмеричную.

\param[in] str[] - число, которое нужно перевести из десятичной системы счисления, преобразовано в строку

\return возвращает строку в восьмеричной системе счисления

*/
QString from10to8 (QString &str)
{
    QString s = "";
    int res = str.toInt();

    if(str == "") return "";

    while(res)
    {
        s.prepend(QString::number(res % 8));
        res /= 8;
    }

    return s;
}
