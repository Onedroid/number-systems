/*!
\file
\brief Файл реализующий перевод чисел из 8 в 10 систему счисления.
\author Designer
\version 1.0
\date 09.06.2019
*/

#include "functions.h"
/*!
\brief Функция перевода из 8 в 10 систему счисления.

\version 1.0

//Полное описание функции:

Функция, выполняющая перевод из восьмеричной системы счисления в десятичную.

\param[in] str[] - число, которое нужно перевести из восьмеричной системы счисления, преобразовано в строку

\return возвращает строку в десятичной системе счисления

*/
QString from8to10 (QString &str)
{
    QString s = "";
    int k = 1, res = 0;

    if(str == "") return "";

    for(int i = str.length()-1; i >= 0; i--)
    {
        res += (str[i].toLatin1()-48) * k;
        k *= 8;
    }
    s = QString::number(res);
    return s;
}
